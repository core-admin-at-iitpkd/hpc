GENERATED=pdf fdb_latexmk fls log aux out tex toc html
.PHONY: clean all tarball
%.tex : %.org
	emacs -Q $< --batch -f org-latex-export-to-latex --kill

%.html: %.org
	emacs -Q $< --batch -f org-html-export-to-html --kill

.DEFAULT_GOAL = all
all: policy.pdf policy.html

clean:
	rm -f $(addprefix policy., ${GENERATED})

policy.pdf: policy.tex
	latexmk -pdf policy.tex

tarball:
	git archive HEAD --format tgz --prefix automation-`git rev-parse HEAD`/ -o automation-`git rev-parse --short HEAD`.tgz
